
import React from 'react';
import Day from './Day';

const Month = ({selectedMonth})=> {
const daysOfWeek = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  const firstDayOfMonth = new Date(2024, selectedMonth - 1, 1).getDay();
const startingDay = daysOfWeek[firstDayOfMonth];

  return (
<div>
<h2>{`Month ${selectedMonth} starts ${startingDay}`}</h2>
<Day startingDay={startingDay}/>
</div>
  );
};
export default Month;
