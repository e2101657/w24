
import React, { useState } from 'react';
import Month from './Month';
const App = () => {
  const [selectedMonth, setSelectedMonth] = useState(1);
  const handleMonthChange = (e) => {
    setSelectedMonth(parseInt(e.target.value));
  };

  return (
    <div>
      <div>
      <select value={selectedMonth} onChange={handleMonthChange}>
  {[...Array(12).keys()].map((month) => (
   <option key={month + 1} value={month + 1}>
     {month + 1}
            </option>
          ))}
        </select>
      </div>
      <Month selectedMonth={selectedMonth} />
</div>
  );
};

export default App;
